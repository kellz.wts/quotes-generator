import React, { Component } from 'react';
import axios from 'axios';
import '../../src/App.css';


export default class QuotesList extends Component {

    constructor(props) {
        super(props);
        this.state = { quotes: [] };
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        if (this.state.quotes.length === 0) this.getQuotes();
    }

    async getQuotes() {
        let res = await axios.get("https://icanhazdadjoke.com/", {
            headers: { Accept: "application/json" }
        });

        let quotes = res.data.joke;
        console.log(quotes);
        this.setState({ quotes: quotes });
    }

    handleClick() {
        this.getQuotes();
    }

    render() {
        return (
            <div className="QuotesList-Wrapper">
                <h1 className="QuotesList-Header">Click for a Dad Joke!</h1>
                <div className="QuotesList-Quotes">
                    {this.state.quotes}
                </div>
                <button className="QuotesList-Button" onClick={this.handleClick}>New Dad Joke</button>
            </div>
        )
    }
}
